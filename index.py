import pyautogui
import time
import pygetwindow as gw
import pydirectinput
import random
import sys, math
import operator
import ctypes
import requests #http webhooks
import json
from datetime import datetime

def GetWindowRectFromName(name:str)-> tuple:
    hwnd = ctypes.windll.user32.FindWindowW(0, name)
    rect = ctypes.wintypes.RECT()
    ctypes.windll.user32.GetWindowRect(hwnd, ctypes.pointer(rect))
    # print(hwnd)
    # print(rect)
    return (rect.left, rect.top, rect.right, rect.bottom)
if __name__ == "__main__":
  windowBox = GetWindowRectFromName('Mabinogi')
  print("windowBox:",windowBox)
  pass

mediaPath = 'media/'
silverImg = mediaPath + 'silver.png'
goldImg = mediaPath + 'gold.png'
mgImg = mediaPath + 'mg.png'
smallMgImg = mediaPath + 'smallMg.png'
#copperOreImg = mediaPath + 'copperOre.png'
#ironOreImg = mediaPath + 'ironOre.png'
#silverOreImg = mediaPath + 'silverOre.png'
oreFragmentImg = mediaPath + 'oreFragment.png'
aquamarineImg = mediaPath + 'aquamarine.png'
starsapphireImg = mediaPath + 'starsapphire.png'
rubyImg = mediaPath + 'ruby.png'
jasperImg = mediaPath + 'jasper.png'

lootList = [oreFragmentImg,aquamarineImg,starsapphireImg]

confid = 0.70
grey = True
seekFails = 0

# center screen coords
hx = math.floor((windowBox[0] + windowBox[2]) / 2)
hy = math.floor((windowBox[1] + windowBox[3]) / 2)

def webHook(desc,title="Metallurgy bot says..."):
  url = "https://discordapp.com/api/webhooks/766225897681715292/vO2xhzuAVxehn8z1Db5wsfxoY-ZEPTTvrPkbYGA3pK0RJvOLstec3VeF6WGj45eLNV4p"
  data = {}
  #for all params, see https://discordapp.com/developers/docs/resources/webhook#execute-webhook
  #data["content"] = "message content"
  data["username"] = "RIDAI"

  #leave this out if you dont want an embed
  data["embeds"] = []
  embed = {}
  #for all params, see https://discordapp.com/developers/docs/resources/channel#embed-object
  embed["description"] = desc
  embed["title"] = title
  data["embeds"].append(embed)

  result = requests.post(url, data=json.dumps(data), headers={"Content-Type": "application/json"})

  try:
      result.raise_for_status()
  except requests.exceptions.HTTPError as err:
      print(err)
  else:
      print("Payload delivered successfully, code {}.".format(result.status_code))

def log(text):
  print(text)
  webHook(text)

def moveEast():
  pyautogui.moveTo(hx+125, hy)
  leftclick()
  sleep(2000)

def moveWest():
  pyautogui.moveTo(hx-125, hy)
  leftclick()
  sleep(3000)

def leftclick(x=None,y=None):
  if x == None and y == None:
    pydirectinput.mouseDown(); time.sleep(.02); pydirectinput.mouseUp()
  else:
    pydirectinput.moveTo(x,y)
    pydirectinput.mouseDown(); time.sleep(.02); pydirectinput.mouseUp()

def sleep(v):
  time.sleep( v * .001 ) #ms

def timeoutExceeded():
  sleep(1000)
  pydirectinput.press("w")

def achievement(achievementValue):
  if (silversCount > 500 and silversCount < 750 and achievementValue == 0):
      achievementValue = 1
      log("Achievement! We have found " + str(silversCount) + " silvers so far!")
  if (silversCount > 750 and achievementValue == 1):
      achievementValue = 2
      log("Achievement! We have found " + str(silversCount) + " silvers so far!")
  return achievementValue

def focusMabi():
	win = gw.getWindowsWithTitle('Mabinogi')[0]
	win.activate()
	sleep(100)

# returns array of target box from image path
def findTargets(targetImg,timeout=0):
  start = time.time()

  targets = list(pyautogui.locateAllOnScreen(targetImg, confidence=confid, grayscale=grey, region=windowBox))
  if (len(targets) < 1):
    i = 0
    while (len(targets) < 1):
      sleep(50)
      i+=1
      if i >= timeout:
        #print("Timed out!")
        return []
  else:
    #end = time.time(); print("findTargets() time taken:",end - start)
    return targets

# returns closest target box in array of boxes
def findClosest(targets):
  if len(targets) < 1:
    return
  lst = list()
  for target in targets:
    xLen = target.left - hx
    yLen = target.top - hy
    lst.append(math.sqrt(xLen*xLen + yLen*yLen))
  try:
    closest = targets[lst.index(min(lst))]
  except:
    return None
  print("Closest target:",closest)
  return closest

# hold alt and click x,y
def clickLoot(x,y):
  pydirectinput.keyDown("alt")
  sleep(50)
  leftclick(x,y)
  sleep(50)
  pydirectinput.keyUp("alt")

# search and click each item in our master loot list
def findAndClickLoot(lootCount=1):
  global lootList
  
  #print("To loot:",lootList)
  for lootImg in lootList:
    i=1
    while i <= lootCount:
      print("> i:",i,"lootCount:",lootCount)
      name = lootImg[len(mediaPath):-4]
      pydirectinput.moveTo(windowBox[0]+100,windowBox[1]+100) # to check loot
      pydirectinput.keyDown("alt")
      print("Looking for",lootImg,"...")
      targets = findTargets(lootImg)
      if targets != False and len(targets) > 0:
        target = targets[0] #findClosest(targets)
        x = target.left
        y = target.top
        clickLoot(x, y+3)
        dist = math.floor(distanceFromCenter(x,y))
        print("Clicking",lootImg,"...")
        pydirectinput.keyUp("alt")
        sleepv = getWaitTime(dist,5000,"l")
        sleep(sleepv) # get loot timer
      else:
        pydirectinput.keyUp("alt")
        print(name,"not found.")
        lootCount += -1
        #return


def locateSilvers():
  # or golds if its raining!
  try:
    start = time.time()
    li = findTargets(goldImg)
    if len(li) > 0:
      print("Found gold(s): ", len(li))
    end = time.time()
    print("locateSilvers() time taken:",round(end - start,2))

    if len(li) < 1:
      start = time.time()
      li = findTargets(silverImg)
      if len(li) > 0:
        print("Found silvers(s): ", len(li))
      end = time.time()
      print("locateSilvers() time taken:",end - start)
    return li
  except:
    print("Couldn't find any silvers/golds!")
    return False

def loadMetallurgy(sleepv=0):
  focusMabi()
  pydirectinput.press('F8')  # use metallurgy
  if sleepv > 0:
    sleep(sleepv)
  
def doMetallurgy(sleepv=0):
  print("doMetallurgy...")
  pyautogui.moveTo(hx,hy-6)
  leftclick()
  if sleepv > 0:
    sleep(sleepv)

def getWaitTime(dist,wait=8000,type="m"):
  if (dist <= 700 and dist > 400):
    wait = wait * .85
  if (dist <= 400 and dist >= 200):
    wait = wait * .65
  if (dist < 200):
    wait = wait * .4 # at least metallurgy load time
    if type == "l":
      wait = 1000
  if (dist < 100):
    wait = 2000
    if type == "l":
      wait = 750
  print("Wait time:",wait,type)
  return wait

def collectNode(target):
  x = random.randint(target.left-5,target.left+5)
  y = random.randint(target.top+5,target.top+10)
  dist = math.floor(distanceFromCenter(x,y))
  pydirectinput.moveTo(x, y+15)
  sleep(5)
  leftclick()
  loadMetallurgy()
  wait = getWaitTime(dist,8000,"m")
  sleep(wait)

def distanceFromCenter(x,y):
  xLen = x - hx
  yLen = y - hy
  dist = math.floor(math.sqrt(xLen*xLen + yLen*yLen))
  print("Distance from center:",dist)
  return dist

### Main
## Loop
runCount = 2000
lootCount = 1
nodeCount = 0

totalFragCount = 0
silversCount = 0
silversCountLast = 0
silversNotFoundCount = 0
achievementStatus = 0

# why not
findAndClickLoot(lootCount)

i=0
while i < runCount:
  focusMabi()

  closestNode = findClosest(locateSilvers())
  if closestNode != None:
    nodeCount = 0
    collectNode(closestNode)
    doMetallurgy(6500)
    # possibly click the loot at our feet?
    leftclick()
    # now loot
    ii=0
    while ii < lootCount:
      findAndClickLoot(lootCount)
      ii+=1
  else:
    nodes = False
    print("No nodes...")
    nodeCount+=1
    if nodeCount == 25:
      moveEast(); moveEast(); moveEast()
    if nodeCount == 50:
      moveWest(); moveWest(); moveWest(); moveWest()
    if nodeCount == 100:
      moveEast(); moveEast()
    if nodeCount == 200:
      log("Haven't seen anything for 100 sec")
      moveEast()
    if nodeCount == 400:
      log("Haven't seen anything for 200 sec")
      moveWest(); moveWest()
    if nodeCount == 600:
      log("Haven't seen anything for 300 sec")
      moveEast(); moveEast(); moveEast()
    if nodeCount == 2000:
      exit()
    print("Failed:",nodeCount)
    sleep(100)
  i+=1
exit()
