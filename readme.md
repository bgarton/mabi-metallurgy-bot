Made with Python 3.8.6 x32

### Install

- `python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose`
- `pip install pyautoit pyautogui pydirectinput requests pygetwindow`
- Fill in your webhook url within index.py

### Run

- Open CMD:
  `py index.py`

### Install
